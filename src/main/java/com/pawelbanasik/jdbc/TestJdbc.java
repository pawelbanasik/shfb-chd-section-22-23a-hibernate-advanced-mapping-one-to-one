package com.pawelbanasik.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {

		String jdbcUrl = "jdbc:mysql://localhost:3306/hb-01-one-to-one-uni?useSSL=false";
		String user = "pawel.banasik";
		String password = "qwertyuiop1";
		
		
		try {

			System.out.println("Connection to database: " + jdbcUrl);
			
			Connection myConn = DriverManager.getConnection(jdbcUrl, user, password);
			
			System.out.println("Connection succesful!");
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
